package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 掲示板リポジトリインターフェイス
 */

public interface BulletInBoardRepository extends JpaRepository<BulletInBoard, Long> {
	public BulletInBoard findById(int id);
	public void deleteById(int id);
}
