package com.example.demo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * 掲示板コントローラ
 */

@Controller
public class DemoController {
	@Autowired
	BulletInBoardRepository repos;
	@Autowired
    DivisionRepository dRepos;

	/* 一覧画面（初期画面）への遷移 */
	@GetMapping("/")
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView();
		List<BulletInBoard> list = repos.findAll();
		// カテゴリIDに対応する名前を格納するリスト
		List<Division> div = new ArrayList<Division>();
		Division divName = null;
		for (int i = 0; i < list.size(); i++) {
			divName = dRepos.findById(list.get(i).getDivision());
			div.add(divName);
		}
		mav.setViewName("list");
		mav.addObject("data", list);
		mav.addObject("divData", div);
		return mav;
	}

	/* 新規投稿画面への遷移 */
	@GetMapping("/add")
	ModelAndView add() {
		ModelAndView mav = new ModelAndView();
		BulletInBoard data = new BulletInBoard();
		mav.addObject("formModel", data);
		mav.setViewName("new");
		//分類テーブルの読み込み
        List<Division> list = dRepos.findAll();
        mav.addObject("lists", list);
		return mav;
	}

	/* 詳細画面への遷移 */
	@GetMapping("/detail")
	ModelAndView detail(@RequestParam int id) {
		ModelAndView mav = new ModelAndView();
		BulletInBoard data = repos.findById(id);
		mav.addObject("formModel", data);
		//分類テーブルの読み込み
        Division div = dRepos.findById(data.getDivision());
        mav.addObject("div", div);
		mav.setViewName("show");
		return mav;
	}

	/* 編集画面への遷移 */
	@GetMapping("/edit")
	ModelAndView edit(@RequestParam int id) {
		ModelAndView mav = new ModelAndView();
		BulletInBoard data = repos.findById(id);
		mav.addObject("formModel", data);
		mav.setViewName("new");
		//分類テーブルの読み込み
        List<Division> list = dRepos.findAll();
        mav.addObject("lists", list);
		return mav;
	}

	/* 更新処理 */
	@PostMapping("/create")
	@Transactional(readOnly=false)
	public ModelAndView save(@ModelAttribute("formModel") BulletInBoard bib) {
		Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        bib.setCreateDate(sdf.format(date));
		repos.saveAndFlush(bib);
		return new ModelAndView("redirect:/");
	}

	/* 削除処理 */
	@PostMapping("/delete")
	@Transactional(readOnly=false)
	public ModelAndView delete(@RequestParam int id) {
		repos.deleteById(id);
		return new ModelAndView("redirect:/");
	}

	/* 初期データ作成 */
	@PostConstruct
	public void init() {
		//掲示板初期データの登録
		BulletInBoard thread1 = new BulletInBoard();
		thread1.setCreateDate("2019/12/01");
		thread1.setTitle("クリスマスプレゼント希望申請について");
		thread1.setContent("クリスマスプレゼントの希望品申請書は、今月10日必着です。遅れた場合にはこちらで選別した品とさせていただきますのでご注意ください。");
		thread1.setCreateUser("サンタクロース広報課");
		thread1.setDivision(1);
		repos.saveAndFlush(thread1);

		BulletInBoard thread2 = new BulletInBoard();
		thread2.setCreateDate("2019/12/18");
		thread2.setTitle("クリスマス中止のお知らせ");
		thread2.setContent("一部トナカイのストライキにより、現在も全世界プレゼント一斉配達の見込みが立っておりません。誠に遺憾ながら、本年のクリスマスは中止とさせていただきます。");
		thread2.setCreateUser("赤鼻のトナカイ");
		thread2.setDivision(3);
		repos.saveAndFlush(thread2);

		//分類テーブル初期データの登録
        Division div1 = new Division();
        div1.setId(1);
        div1.setName("お知らせ");
        dRepos.saveAndFlush(div1);

        Division div2 = new Division();
        div2.setId(2);
        div2.setName("スケジュール");
        dRepos.saveAndFlush(div2);

        Division div3 = new Division();
        div3.setId(3);
        div3.setName("イベント");
        dRepos.saveAndFlush(div3);

        Division div4 = new Division();
        div4.setId(4);
        div4.setName("その他");
        dRepos.saveAndFlush(div4);
	}
}
