package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 利用者リポジトリインターフェース
 */

public interface UserRepository extends JpaRepository<User, String> {
	User findByUsername(String username);
}
