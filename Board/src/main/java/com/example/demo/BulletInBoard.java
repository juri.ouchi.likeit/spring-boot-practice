package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 掲示板エンティティクラス
 */

@Entity
@Table(name="BulletinBoard")
public class BulletInBoard {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private int id;
    @Column
    private String createDate; // 作成日
	@Column
	private String title; // タイトル
	@Column
	private String content; // 内容
	@Column
	private String createUser; // 作成者
	@Column
	private int division; // カテゴリ

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public int getDivision() {
		return division;
	}
	public void setDivision(int division) {
		this.division = division;
	}
}
