package com.example.demo;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * ログイン認証コントローラ
 */

@Controller
public class LoginController {
	@Autowired
	UserRepository repos;
	@Autowired
	LoginUserDetailsService loginUserDetailsService;
	@Autowired
	PasswordEncoder passwordEncoder;

	@GetMapping(path = "loginForm")
	String loginForm() {
		return "loginForm";
	}

	/* 新規登録画面への遷移  */
	@GetMapping("/userAdd")
	ModelAndView create() {
		//System.out.println("1111");
		ModelAndView mav = new ModelAndView();
		User user = new User();
		mav.addObject("registForm", user);
		List<User> list = repos.findAll();
		mav.addObject("data", list);
		mav.setViewName("userAdd");
		return mav;
	}

	/* 登録処理 */
	@PostMapping("/userCreate")
	@Transactional(readOnly=false)
	public ModelAndView save(@ModelAttribute("registForm") User user) {
		System.out.println(user.getUsername());
		System.out.println(user.getEncodedPassword());
		user.setEncodedPassword(passwordEncoder.encode(user.getEncodedPassword()));
		//loginUserDetailsService.create(user, user.getEncodedPassword());
		repos.saveAndFlush(user);
		return new ModelAndView("redirect:/loginForm");
	}

	/* 初期データ作成 ユーザ:demo パスワード:demo */
	@PostConstruct
	public void init() {
		User user1 = new User();
		user1.setUsername("demo");
		user1.setEncodedPassword("7506c69384e000e6abb1de01165788de9f450cc69b0be847636d37c6278cefa69a042b40092a2d64");
		repos.saveAndFlush(user1);
	}
}
