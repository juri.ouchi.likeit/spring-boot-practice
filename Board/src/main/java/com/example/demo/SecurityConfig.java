package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * 認証関係の環境設定
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	public void configure(WebSecurity web) throws Exception {
		// セキュリティ設定を無視するリクエスト設定(静的リソースへのアクセス)
		web.ignoring().antMatchers("/webjars/**", "/css/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			// アクセス権限の設定
	        // アクセス制限の無いURL
			.antMatchers("/loginForm").permitAll()
			.antMatchers("/userAdd").permitAll()
			.antMatchers("/userCreate").permitAll()
			// その他は認証済みであること
			.anyRequest().authenticated()
			.and()
		.formLogin()
			// リクエストされるとログイン認証の処理を行うURL
			.loginProcessingUrl("/login")
			// ログイン画面
			.loginPage("/loginForm")
			// ログイン失敗時の遷移先
			.failureUrl("/loginForm?error")
			// ログイン成功時の遷移先
			.defaultSuccessUrl("/", true)
			// ログイン画面のusernameのパラメータ名
			.usernameParameter("username")
			// ログイン画面のpasswordのパラメータ名
			.passwordParameter("password")
			.and()
		.logout()
			// ログアウト処理
	        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			// ログアウト成功時の遷移先
			.logoutSuccessUrl("/loginForm");
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return new Pbkdf2PasswordEncoder();
	}
}
